import java.lang.annotation.*;

@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Naval {
    String country() default "Britain";
    //specialization (type)
    boolean isCargo() default true;
    int count() default 1;
}
