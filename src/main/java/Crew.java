public class Crew {

    @Naval (country = "Ukraine", isCargo = false)
    String  captain;
    @Naval (count = 5)
    String[] sailors;
    int overallNumber;

    public Crew() {}

    public Crew(String  captain, String[] sailors, int overallNumber) {
        this.captain = captain; this.sailors = sailors; this.overallNumber = overallNumber;
    }

    public String getCaptain() {
        return captain;
    }

    public void setCaptain(String captain) {
        this.captain = captain;
    }

    public String[] getSailors() {
        return sailors;
    }

    public void setSailors(String[] sailors) {
        this.sailors = sailors;
    }

    public int getOverallNumber() {
        return overallNumber;
    }

    public void setOverallNumber(int overallNumber) {
        this.overallNumber = overallNumber;
    }

    public int addSailorsandCaptainSalaries(String string, int...params) {
        System.out.print(string);
        int sum = 0;
        for(int i:params) {sum+=i;}
        return sum;
    }

    public void showSailorsNames(String...params) {
        System.out.print("The sailor names are:");
        for(String j:params) {System.out.print(" --"+j);
        }
        System.out.println();
    }

    public String showInfoAboutClass(int n) {
        return new String("The Crew class describe the leading, sailing proffesional, and auxilliary members of crews, and " +n+ " annotation fields in class");
    }

}