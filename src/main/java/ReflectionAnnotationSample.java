import java.lang.reflect.Field;
import java.lang.reflect.Method;
import static java.util.Objects.requireNonNull;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.lang.reflect.InvocationTargetException;

public class ReflectionAnnotationSample {

    public static void showAnnotationSamples() throws IllegalAccessException {
        Crew crew = new Crew("V.Shevchenko", new String[] {"M.Smith", "R.Owen", "J.Cargill", "D. Mason", "A.Donowan"}, 20);
        Class<?> objectClass = requireNonNull(crew).getClass();
        System.out.println("Object class is: "+ objectClass.getCanonicalName());
        for (Field field: objectClass.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(Naval.class)) {
                Naval anots = (Naval) field.getAnnotation(Naval.class);
                String country = anots.country();
                boolean isCargo = anots.isCargo();
                int count = anots.count();
                System.out.println("Annotated Field: " + field.getName());
                System.out.println("@Naval country: " + country);
                System.out.println("@Naval isCargo: " + isCargo);
                System.out.println("@Naval count: " + count);
            } else System.out.println("Field - " + field.getName() + ": is not annotated");
        }
            //Set value into field not knowing its type with Reflection
            Scanner sc=new Scanner(System.in);
            System.out.println("Do you want to set integer value for unknown field of the object, if any - type 'y' and return");
            while(sc.hasNextLine()) {
                if (sc.nextLine().equals("y")) {
                    System.out.println("Type integer value and return");
                    int num = 0;
                    try{
                        num = sc.nextInt();
                    }
                    catch (InputMismatchException ex) {System.out.println("Typing incorrect non-integer number");
                        return;}
                    for (Field field1: objectClass.getDeclaredFields()) {
                        field1.setAccessible(true);
                        if (field1.getType() == int.class) {
                            field1.setInt(crew, num);
                            System.out.println("New value - " + field1.getInt(crew)+ " set for field - " + field1.getName());
                            break;
                        }
                    }
                }
                else return;
            }
            sc.close();
    }

    public static void invokeMethodsWithReflection() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException{
        Crew crew = new Crew("V.Shevchenko", new String[] {"M.Smith", "R.Owen", "J.Cargill", "D. Mason", "A.Donowan"}, 20);
        Class<?> objectClass = requireNonNull(crew).getClass();
        System.out.println("Invoke 3 standart methods with reflection");
        System.out.println("Invoke String showInfoAboutClass(int n) -- ");
        Method method1 = objectClass.getDeclaredMethod("showInfoAboutClass", new Class[]{java.lang.Integer.TYPE});
        String val1 = (String)method1.invoke(crew, new Object[]{2});
        System.out.println(val1);
        System.out.print("Invoke int getOverallNumber() -- ");
        Method method2 = objectClass.getDeclaredMethod("getOverallNumber");
        int val2 = (int)method2.invoke(crew);
        System.out.println(val2);
        System.out.print("Invoke void setCaptain(String s) -- ");
        Method method3 = objectClass.getDeclaredMethod("setCaptain", new Class[]{String.class});
        method3.invoke(crew, new Object[]{"K.Hryhorenko"});
        System.out.println("Check the new name that was set: " + crew.getCaptain());
        System.out.println("Invoke methods with variable arguments");
        System.out.println("Invoke showSailorsNames(String...params) --");
        Method  method4 = objectClass.getMethod ("showSailorsNames", String[].class);
        method4.invoke(crew, (Object) new String[]{"Andrew","Brian","Cameron","Derick","Edward"});
        System.out.println("Invoke addSailorsandCaptainSalaries(String string, int...params) --");
        Method  method5 = objectClass.getMethod ("addSailorsandCaptainSalaries", new Class[]{String.class, int[].class});
        int salary=(int)method5.invoke(crew, new Object[]{"Overall salary of crew sailors and captain", new int[]{ 2,4,5,6,7,9}});
        System.out.println("-"+salary);
    }


    public static void main (String [] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("To show annotation sample - press 'a' ");
        System.out.println("To show invoking method sample with reflection -press 'i' ");
        System.out.println("To show info of unknown class object accepted by other class  -press 'p' ");
        while(sc.hasNextLine()) {
            String option = sc.nextLine();
            if (option.equals("a")) {showAnnotationSamples();}
            else if (option.equals("i")) {invokeMethodsWithReflection();}
            else if (option.equals("p")) {
                AcceptingClass<Crew> obj = new AcceptingClass<>(Crew.class);
            }
            else break;
            System.out.println("Choose another task -- or Return");
        }
        sc.close();
    }

}
