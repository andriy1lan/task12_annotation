import java.lang.reflect.Field;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class AcceptingClass<T> {

    private final Class<T> clazz;

    public AcceptingClass(Class<T> clazz) {
        this.clazz = clazz;
        System.out.println("The name of class is " + clazz.getSimpleName());
        for (Constructor constructor: clazz.getConstructors()) {
            System.out.println("The number of parameters in constructor is " + constructor.getParameterCount()); }
        System.out.println("The number of methods is " + clazz.getDeclaredMethods().length);
        for (Method method: clazz.getDeclaredMethods()) {
            System.out.println("The method name is " + method.getName()); }

        System.out.print("The fields of class are : ");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.print("  " + field.getName());
        }
        System.out.println();

    }
}